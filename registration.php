<?php
include("header.php")
?>

<style>
    
    .inner-banner-wrapper {
    text-align: center;
    padding: 130px 0 30px;
}

.hm{
	color: white;
}

.rleft {
    float: left;
    height: 55px;
    width: 55px;
    margin-right: 30px;
}

.rleft img{
	padding-top: 0px;
	width: 100%;
	height: 100%;
}

.rright{
	width: 100%;
}

.rright p{

}

.fea{
	padding: 50px 10px 50px 0px;
}


.left{
		padding-top: 50px;
		padding-bottom: 50px;
	}

	.right{
		padding: 50px 0px;
	}

	.sectionl{
		padding: 0px 0px;
		width: 100%;
		
	}

	   .inner-banner-wrapper {
    text-align: center;
    padding: 130px 0 30px;
	}

	.container{
		border-bottom: 1px solid #e8e8e8;
	}
	li{
		padding: 5px 0px;
	}

	.sec h2{
		float: right;
	}

	.sec ul{
		float: right;
	}

	.sec ul li{
		float: right;
	}

	.fl{
		float: right;
	}


@media(max-width: 468px){
	  .inner-banner-wrapper {
    text-align: center;
    padding: 50px 0 30px;
	}

	.flm{
		float: right;
		margin-top: 20px;
	}

	.right{
		padding-top: 0px;
	}

	.sec ul li{
		float: left;
	}
}

</style>

            <!-- Main Banner 1 Area Start Here -->
            <div class="inner-banner-area">
                <div class="container">
                    <div class="inner-banner-wrapper">
                        <h2 class="hm">About LegaConnect</h2>
                        <p>Managing client cases from collation of facts, drafting, case filing, and client support</p>
                        
                    </div>
                </div>
            </div>
			<!-- Main Banner 1 Area End Here --> 

			<div class="row" style="padding-top: 30px;">
				<div class="container">
					<div class="col-md-4 fea" >
						<div class="rleft">
							<img src="img/website.png">
						</div>
						<div class="rright">
							<h3 style="line-height: 0.5;">Save Time & Money</h3>
							<p>A digital office assistant that increases your productivity!</p>
						</div>
					</div>

					<div class="col-md-4 fea">
						<div class="rleft">
							<img src="img/website.png">
						</div>
						<div class="rright">
							<h3 style="line-height: 0.5;">Save Time & Money</h3>
							<p>A digital office assistant that increases your productivity!</p>
						</div>
					</div>

					<div class="col-md-4 fea">
						<div class="rleft">
							<img src="img/website.png">
						</div>
						<div class="rright">
							<h3 style="line-height: 0.5;">Save Time & Money</h3>
							<p>A digital office assistant that increases your productivity!</p>
						</div>
					</div>

				</div>
                    
            </div>

            <div class="row" style="padding-bottom: 30px;">
				<div class="container">
					<div class="col-md-4 fea">
						<div class="rleft">
							<img src="img/website.png">
						</div>
						<div class="rright">
							<h3 style="line-height: 0.5;">Save Time & Money</h3>
							<p>A digital office assistant that increases your productivity!</p>
						</div>
					</div>

					<div class="col-md-4 fea">
						<div class="rleft">
							<img src="img/website.png">
						</div>
						<div class="rright">
							<h3 style="line-height: 0.5;">Save Time & Money</h3>
							<p>A digital office assistant that increases your productivity!</p>
						</div>
					</div>

					<div class="col-md-4 fea">
						<div class="rleft">
							<img src="img/website.png">
						</div>
						<div class="rright">
							<h3 style="line-height: 0.5;">Save Time & Money</h3>
							<p>A digital office assistant that increases your productivity!</p>
						</div>
					</div>

				</div>
                    
            </div>



            <!-- Inner Page Banner Area Start Here -->
           <!--  <div class="pagination-area bg-secondary">
                <div class="container">
                    <div class="pagination-wrapper">
                        <ul>
                            <li><a href="index.php">Home</a><span> -</span></li>
                            <li>Registration</li>
                        </ul>
                    </div>
                </div>  
            </div>  -->
            <!-- Inner Page Banner Area End Here -->          
            <!-- Registration Page Area Start Here -->
            <div class="registration-page-area bg-secondary section-space-bottom" style="padding-top: 60px;">
                <div class="container">
                    <h2 class="title-section" style="text-align: center;">Registration</h2>
                    <div class="registration-details-area inner-page-padding">
                        <form id="personal-info-form">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">                                          
                                    <div class="form-group">
                                        <label class="control-label" for="first-name">First Name *</label>
                                        <input type="text" id="first-name" class="form-control">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">                                          
                                    <div class="form-group">
                                        <label class="control-label" for="last-name">Last Name *</label>
                                        <input type="text" id="last-name" class="form-control">
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">                                          
                                    <div class="form-group">
                                        <label class="control-label" for="email">E-mail Address *</label>
                                        <input type="text" id="email" class="form-control">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">                                          
                                    <div class="form-group">
                                        <label class="control-label" for="phone">Phone *</label>
                                        <input type="text" id="phone" class="form-control">
                                    </div>
                                </div>
                            </div>                                  
                            
                            
                            <div class="row">
                                
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">                                          
                                    <div class="form-group">
                                        <label class="control-label" for="postcode">City</label>
                                        <input type="text" id="city" class="form-control">
                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">                                          
                                    <div class="form-group">
                                        <label class="control-label" for="postcode">Postcode / ZIP</label>
                                        <input type="text" id="postcode" class="form-control">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">                                           
                                    <div class="form-group">
                                        <label class="control-label">Address</label>
                                        <input type="text" id="address-line1" class="form-control">
                                      
                                    </div>
                                </div>                                      
                            </div>

                                <div class="row">

                                 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">                                           
                                    <div class="pLace-order">
                                        <button class="update-btn disabled" type="submit" value="Login">Submit</button>
                                    </div>
                                </div>
                            </div> 
                        </form>                      
                    </div> 
                </div>
            </div>
            <!-- Registration Page Area End Here -->


            	<div class="row sectionl"> 
					<div class="container">
					<div class="col-md-7 col-sm-6 left">
						<h2>Heading sample</h2>
						<ul>
							
							<li>&#8226;&nbsp;&nbsp; Managing client cases from collation of facts, drafting, case filing, and client support</li>
							<li>&#8226;&nbsp;&nbsp;Storing & accessing case documents remotely</li>
							<li>&#8226;&nbsp;&nbsp;Access to 100,000+ consumers/month looking to redress legal grievances</li>
							<li>&#8226;&nbsp;&nbsp;No opportunity of networking professionally</li>
						</ul>
					</div>

					<div class="col-md-5 col-sm-6 right" >
						<img src="img/tutorial/3.jpg" class="fl">
					</div>
					</div>
				</div>


				<div class="row sectionl"> 
					<div class="container">

						<div class="col-md-5 col-sm-6 right">
						<img src="img/tutorial/3.jpg" class="flm">
					</div>

					<div class="col-md-7 col-sm-6 left sec">
						<h2>Heading sample</h2>
						<ul>
							
							<li>Managing client cases from collation of facts, drafting, case filing, and client support&nbsp;&nbsp;&#8226;</li>
							<li>Storing & accessing case documents remotely&nbsp;&nbsp;&#8226;</li>
							<li>Access to 100,000+ consumers/month looking to redress legal grievances&nbsp;&nbsp;&#8226;</li>
							<li>No opportunity of networking professionally&nbsp;&nbsp;&#8226;</li>
						</ul>
					</div>

					
					</div>
				</div>


				<div class="row sectionl"> 
					<div class="container">
					<div class="col-md-7 col-sm-6 left">
						<h2>Heading sample</h2>
						<ul>
							
							<li>&#8226;&nbsp;&nbsp; Managing client cases from collation of facts, drafting, case filing, and client support</li>
							<li>&#8226;&nbsp;&nbsp;Storing & accessing case documents remotely</li>
							<li>&#8226;&nbsp;&nbsp;Access to 100,000+ consumers/month looking to redress legal grievances</li>
							<li>&#8226;&nbsp;&nbsp;No opportunity of networking professionally</li>
						</ul>
					</div>

					<div class="col-md-5 col-sm-6 right">
						<img src="img/tutorial/3.jpg" class="fl">
					</div>
					</div>
				</div>
<?php

include("footer.php");
?>