<?php
include("header.php");
?>
<style>
    
    .inner-banner-wrapper {
    text-align: center;
    padding: 130px 0 30px;
}

.product-list-view .single-item-list .item-content .item-info .item-title{
    -webkit-box-flex: 3;
    
    -webkit-flex: 3;
    flex: 3;
}

    .item-title .exp{
        padding-top: 20px;

    }

    .page-controls-sorting{
        float: right;
    }

    .product-list-view .single-item-list{
        box-shadow: 0px 0px 2px 0px rgba(205, 214, 222, 0.75);
    }

    .product-list-view .single-item-list .item-img{
        -webkit-box-flex: 3;
    -moz-flex: 3;
    -webkit-flex: 3;
    flex: 3;
    position: relative;
    background-color: white;
    box-shadow: 0px 0px 0px 0px rgba(205, 214, 222, 0.75);
    }

    .product-list-view .single-item-list .item-content{
        box-shadow: 0px 0px 0px 0px rgba(205, 214, 222, 0.75);
    }

    .product-list-view .single-item-list .item-img img{
        padding-top: 15px;
    padding-left: 15px;
    }

    .form-group{
    	border-bottom: 0.5px solid rgba(234, 195, 86,0.9);
    }

</style>

            <!-- Main Banner 1 Area Start Here -->
            <div class="inner-banner-area">
                <div class="container">
                    <div class="inner-banner-wrapper">
                        <p>GET INSTANT ACCESS TO EXPERIENCED LAWYERS</p>
                        <div class="banner-search-area input-group">
                            <input class="form-control" placeholder="Search Your Keywords . . ." type="text">
                            <span class="input-group-addon">
                                <button type="submit">
                                    <span class="glyphicon glyphicon-search"></span>
                                </button>  
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Main Banner 1 Area End Here --> 
                    
  			<!-- Settings Page Start Here -->
            <div class="settings-page-area bg-secondary section-space-bottom" style="padding-top: 20px;">
                <div class="container">
                    <div class="row settings-wrapper">
                        
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"> 
                            <form class="form-horizontal" id="personal-info-form">
                                <div class="settings-details tab-content">
                                    <div class="tab-pane fade active in" id="Personal">
                                        <h2 class="title-section">Account Details</h2>
                                        <div class="personal-info inner-page-padding"> 
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">First Name - </label>
                                                <div class="col-sm-9">
                                                    <h4>Nikileen</h4>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Last Name - </label>
                                                <div class="col-sm-9">
                                                    <h4>Kamble</h4>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Email Address - </label>
                                                <div class="col-sm-9">
                                                    <h4>n@gmail.com</h4>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Contact Number - </label>
                                                <div class="col-sm-9">
                                                    <h4>9762534668</h4>
                                                </div>
                                            </div>
                                            
                                            
                                            
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">City - </label>
                                                <div class="col-sm-9">
                                                    <h4>Pune</h4>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Zip / Postal Code - </label>
                                                <div class="col-sm-9">
                                                    <h4>411014</h4>
                                                </div>
                                            </div> 

                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">Address - </label>
                                                <div class="col-sm-9">
                                                    <h4>Viman nagar, Pune.</h4>
                                                </div>
                                            </div>

                                        </div> 
                                    </div> 
                                   
                                   
                                                                          
                                </div> 

                            </form> 
                        </div>  
                    </div>  
                </div>  
            </div> 
            <!-- Settings Page End Here -->


 <?php
 include("footer.php");
 ?>