<?php
include("header.php");
?>
<style>
    
    .inner-banner-wrapper {
    text-align: center;
    padding: 130px 0 30px;
}

.product-list-view .single-item-list .item-content .item-info .item-title{
    -webkit-box-flex: 3;
    
    -webkit-flex: 3;
    flex: 3;
}

    .item-title .exp{
        padding-top: 9%;

    }

    .page-controls-sorting{
        float: right;
    }

    .product-list-view .single-item-list{
        box-shadow: 0px 0px 2px 0px rgba(205, 214, 222, 0.75);
    }

    .product-list-view .single-item-list .item-img{
    -webkit-box-flex: 3;
    -moz-flex: 3;
    -webkit-flex: 3;
    flex: 3;
    position: relative;
    background-color: white;
    box-shadow: 0px 0px 0px 0px rgba(205, 214, 222, 0.75);
    }

    .product-list-view .single-item-list .item-content{
        box-shadow: 0px 0px 0px 0px rgba(205, 214, 222, 0.75);
    }

    .product-list-view .single-item-list .item-img img{
        padding: 15px;
    }

    .product-list-view .single-item-list .item-content .item-info{
        border: 0px solid #fff;
    }

    .item-title p{
        display: block !important;
    }

    .sale-qty{
        display: block !important;
        padding-top: 9%;
    }


    .section-space-bottom{
        padding: 0 0 50px;
    }

    .product-details-content li img{
        width: 50px;
        height: auto;
        padding-right: 10px;
        margin-bottom: 10px;
    }
    .qdet{
        font-size: 14px;
    padding-left: 20px;
    color: grey;
    }

    .qhead{
        font-weight: 500;
    }

    .contact-us-info-area {
    padding: 100px 0 125px;
    }

    @media(max-width:768px){

    .product-list-view .single-item-list .item-content .item-info .item-sale-info{
        flex: 2;
    }
    }
</style>

            <!-- Main Banner 1 Area Start Here -->
            <div class="inner-banner-area">
                <div class="container">
                    <div class="inner-banner-wrapper">
                        <p>GET INSTANT ACCESS TO EXPERIENCED LAWYERS</p>
                        <div class="banner-search-area input-group">
                            <input class="form-control" placeholder="Search Your Keywords . . ." type="text">
                            <span class="input-group-addon">
                                <button type="submit">
                                    <span class="glyphicon glyphicon-search"></span>
                                </button>  
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Main Banner 1 Area End Here --> 

             <!-- Inner Page Banner Area Start Here -->
            <div class="pagination-area bg-secondary">
                <div class="container">
                    <div class="pagination-wrapper">
                        <ul>
                            <li><a href="index.php">Home</a><span> -</span></li>
                            <li>Contact Us</li>
                        </ul>
                    </div>
                </div>  
            </div> 
            <!-- Inner Page Banner Area End Here -->     

            <!-- Contact Us Info Area Start Here -->
            <div class="contact-us-info-area">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <div class="contact-us-left">
                                <h2>Contact Information</h2>      
                                <ul>
                                    <li>
                                        <i class="fa fa-map-marker" aria-hidden="true"></i>
                                        <h3 class="title-bar-medium-left">Our Office Address</h3>
                                        <p>Office No. 702, B Wing, East Court, Phoenix Market City, Viman Nagar, Pune, Maharashtra 411014</p> 
                                    </li>
                                    <li>
                                        <i class="fa fa-phone" aria-hidden="true"></i>
                                        <h3 class="title-bar-medium-left">Phone</h3>
                                        <p>020 4860 1600</p>   
                                    </li>
                                    <li>
                                        <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                        <h3 class="title-bar-medium-left">E-mail</h3>
                                        <p>contactus@desteksolutions.com</p>   
                                    </li>      
                                </ul>
                            </div>  
                        </div>  
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                            <div class="contact-us-right"> 
                                <h2>Drop Us A Message </h2>    
                                <div class="contact-form"> 
                                    <form id="contact-form">
                                        <fieldset>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <input type="text" placeholder="Name*" class="form-control" name="name" id="form-name" data-error="Name field is required" required>
                                                        <div class="help-block with-errors"></div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <input type="email" placeholder="Email*" class="form-control" name="email" id="form-email" data-error="Email field is required" required>
                                                        <div class="help-block with-errors"></div>
                                                    </div>
                                                </div>
                                                 <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="text" placeholder="Subject*" class="form-control" name="subject" id="form-subject" data-error="Subject field is required" required>
                                                        <div class="help-block with-errors"></div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <textarea placeholder="Message*" class="textarea form-control" name="message" id="form-message" rows="7" cols="20" data-error="Message field is required" required></textarea>
                                                        <div class="help-block with-errors"></div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-sm-6 col-sm-12">
                                                    <div class="form-group margin-bottom-none">
                                                        <button type="submit" class="update-btn">Send Message</button>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-6 col-sm-12">
                                                    <div class='form-response'></div>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Contact Us Page Area End Here -->

            <!-- Google Map Area Start Here -->
            <div class="google-map-area">                               
                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15128.949448719075!2d73.917772!3d18.5633346!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x7f177adafc9c36c3!2sDestek+Infosolutions+Pvt+Ltd!5e0!3m2!1sen!2sin!4v1564465814928!5m2!1sen!2sin" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>                           
            </div>
            <!-- Google Map Area End Here -->          
            

             <?php
 include("footer.php");
 ?>