<?php
include("header.php");
?>
<style>
    
    .inner-banner-wrapper {
    text-align: center;
    padding: 130px 0 30px;
}

.product-list-view .single-item-list .item-content .item-info .item-title{
    -webkit-box-flex: 3;
    
    -webkit-flex: 3;
    flex: 3;
}

    .item-title .exp{
        padding-top: 9%;

    }

    .page-controls-sorting{
        float: right;
    }

    .product-list-view .single-item-list{
        box-shadow: 0px 0px 2px 0px rgba(205, 214, 222, 0.75);
    }

    .product-list-view .single-item-list .item-img{
    -webkit-box-flex: 3;
    -moz-flex: 3;
    -webkit-flex: 3;
    flex: 3;
    position: relative;
    background-color: white;
    box-shadow: 0px 0px 0px 0px rgba(205, 214, 222, 0.75);
    }

    .product-list-view .single-item-list .item-content{
        box-shadow: 0px 0px 0px 0px rgba(205, 214, 222, 0.75);
    }

    .product-list-view .single-item-list .item-img img{
        padding: 15px;
    }

    .product-list-view .single-item-list .item-content .item-info{
        border: 0px solid #fff;
    }

    .item-title p{
        display: block !important;
    }

    .sale-qty{
        display: block !important;
        padding-top: 9%;
    }


    .section-space-bottom{
        padding: 0 0 50px;
    }

    .product-details-content li img{
        width: 50px;
        height: auto;
        padding-right: 10px;
        margin-bottom: 10px;
    }
    .qdet{
        font-size: 14px;
    padding-left: 20px;
    color: grey;
    }

    .qhead{
        font-weight: 500;
    }

    @media(max-width:768px){

    .product-list-view .single-item-list .item-content .item-info .item-sale-info{
        flex: 2;
    }
    }
</style>

            <!-- Main Banner 1 Area Start Here -->
            <div class="inner-banner-area">
                <div class="container">
                    <div class="inner-banner-wrapper">
                        <p>GET INSTANT ACCESS TO EXPERIENCED LAWYERS</p>
                        <div class="banner-search-area input-group">
                            <input class="form-control" placeholder="Search Your Keywords . . ." type="text">
                            <span class="input-group-addon">
                                <button type="submit">
                                    <span class="glyphicon glyphicon-search"></span>
                                </button>  
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Main Banner 1 Area End Here --> 
            <!-- Inner Page Banner Area Start Here -->
            <div class="pagination-area bg-secondary">
                <div class="container">
                    <div class="pagination-wrapper">
                        <ul>
                            <li><a href="index.php">Home</a><span> -</span></li>
                            <li>Lawyer Name</li>
                        </ul>
                    </div>
                </div>  
            </div> 
            <!-- Inner Page Banner Area End Here -->          
            <!-- Product Page Grid Start Here -->
            <div class="product-page-list bg-secondary section-space-bottom">                
                <div class="container">
                    <div class="row">                        
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="inner-page-main-body">
                              
                                <div class="tab-content">
                                    
                                    <div role="tabpanel" class="tab-pane fade in active clear products-container" id="list-view">
                                        <div class="product-list-view">
                                            <div class="single-item-list">
                                                <div class="item-img">
                                                    <img src="img/product/36.jpg" alt="product" class="img-responsive">
                                                    
                                                </div>
                                                <div class="item-content">
                                                    <div class="item-info">
                                                        <div class="item-title">
                                                            <h3><a href="#" style="font-size: 18px;">Sample Name</a></h3>
                                                            <span>Designation</span>
                                                            <h3 class="exp">EXPERTISE IN</h3>
                                                            <p>Property Law, Employment, Criminal, Cyber Crime, Immigration Services, Insurance, Corporate, IPR, Startup, Alternate Dispute Resolution, Environment Law, Family </p>
                                                        </div>
                                                        <div class="item-sale-info">
                                                            <div class="price">
                                                                
                                                                <ul class="sidebar-product-btn">
                                                                <li> <a href="#" class="add-to-cart-btn" id="cart-button">Enquire Now</a></li>
                                                                </ul>

                                                            </div>
                                                            <div class="sale-qty"><span style="color: black;font-size: 20px;">4+</span><br>Years of Practice</div>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                            
                                           
                                            
                                           
                                            
                                            
                                            
                                        </div>
                                    </div>                               
                                </div>                                
                            </div>
                        </div>
                        



                    </div>
                </div>
            </div>
            <!-- Product Page Grid End Here -->


            <div class="product-details-page bg-secondary">                
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="inner-page-main-body">


                                <div class="product-details-tab-area">
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            <ul class="product-details-title">
                                                <li class="active"><a href="#description" data-toggle="tab" aria-expanded="false">Expertise</a></li>
                                                <li><a href="#review" data-toggle="tab" aria-expanded="false">Address</a></li>
                                                <li><a href="#qua" data-toggle="tab" aria-expanded="false">Qualification</a></li>
                                                <li><a href="#qa" data-toggle="tab" aria-expanded="false">Q&A</a></li>
                                            </ul>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            <div class="tab-content">
                                                <div class="tab-pane fade active in" id="description">
                                                    <ul class="product-details-content">
                                                        <li><img src="img/icon.png"> Porem ipsum dolor sit amet</li>
                                                        <li><img src="img/icon.png"> Porem ipsum dolor sit amet</li>
                                                        <li><img src="img/icon.png"> Porem ipsum dolor sit amet</li>
                                                        <li><img src="img/icon.png"> Porem ipsum dolor sit amet</li>
                                                        <li><img src="img/icon.png"> Porem ipsum dolor sit amet</li>
                                                        <li><img src="img/icon.png"> Porem ipsum dolor sit amet</li>
                                                        <li><img src="img/icon.png"> Porem ipsum dolor sit amet</li>
                                                        <li><img src="img/icon.png"> Porem ipsum dolor sit amet</li>
                                                        
                                                    </ul>
                                                </div>
                                                <div class="tab-pane fade" id="review">
                                                    <h4>City - Pune</h4>
                                                    <p>Porem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. </p>                      
                                                </div>
                                                <div class="tab-pane fade" id="qua">                           
                                                 
                                                <div class="help-page-area bg-secondary section-space-bottom" style="padding-bottom: 0px;">
                                                    <div class="">
                                                        
                                                        <div class="inner-page-details inner-page-padding" style="padding: 0px;"> 
                                                            <div class="panel-group help-page-wrapper" id="accordion" style="margin-bottom: 0px;">
                                                                <div class="panel panel-default" style="margin-bottom: 0px;">
                                                                    <div class="panel-heading" style="background-color: #f6f6f4 !important;border-top: 1px solid #ddd;">
                                                                        <div class="panel-title">
                                                                            <a aria-expanded="false" class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" style="color: #263238">
                                                                               <img src="img/icon.png" style="padding-right: 30px;"><span class="qhead">Courts</span> <span class="qdet">District Court, Pune Magistrate Court</span>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                    <div aria-expanded="false" id="collapseOne" role="tabpanel" class="panel-collapse collapse">
                                                                        <div class="panel-body">
                                                                            <h4 style="font-weight: 500;font-size: 18px !important;">&#10095;&nbsp;&nbsp;District Court</h4>
                                                                            <h4 style="font-weight: 500;font-size: 18px !important;">&#10095;&nbsp;&nbsp;Pune Magistrate Court</h4>
                                                                            
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                
                                                                
                                                                                          
                                                            </div>
                                                        </div>  
                                                    </div>  
                                                </div> 
                                                
                                                </div>  

                                                <div class="tab-pane fade" id="qa"> 

                                                <!-- Help Page Start Here -->
            <div class="help-page-area bg-secondary section-space-bottom">
                <div class="">
                    <h2 class="title-section">How can we help?</h2>
                    <div class="inner-page-details inner-page-padding"> 
                        <div class="panel-group help-page-wrapper" id="accordion">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        <a aria-expanded="false" class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                           #  Sample Heading
                                        </a>
                                    </div>
                                </div>
                                <div aria-expanded="false" id="collapseOne" role="tabpanel" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <h3>How To Buy A Product?</h3>
                                        <p>when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centurbut also survived but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the releas survived not  raseth leap into electronic typesetting, remaining essentially unchanged. </p>
                                        <h3>How To Get Product Support?</h3>
                                        <p>when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centurbut also survived but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the releas survived not  raseth leap into electronic typesetting, remaining essentially unchanged. </p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        <a aria-expanded="false" class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                          #  Sample Heading
                                        </a>
                                    </div>
                                </div>
                                <div aria-expanded="false" id="collapseTwo" role="tabpanel" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                         <h3>How To Buy A Product?</h3>
                                        <p>when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centurbut also survived but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the releas survived not  raseth leap into electronic typesetting, remaining essentially unchanged. </p>
                                        <h3>How To Get Product Support?</h3>
                                        <p>when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centurbut also survived but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the releas survived not  raseth leap into electronic typesetting, remaining essentially unchanged. </p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        <a aria-expanded="false" class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                           #  Sample Heading
                                        </a>
                                    </div>
                                </div>
                                <div aria-expanded="false" id="collapseThree" role="tabpanel" class="panel-collapse collapse">
                                    <div class="panel-body">
                                         <h3>How To Buy A Product?</h3>
                                        <p>when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centurbut also survived but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the releas survived not  raseth leap into electronic typesetting, remaining essentially unchanged. </p>
                                        <h3>How To Get Product Support?</h3>
                                        <p>when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centurbut also survived but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the releas survived not  raseth leap into electronic typesetting, remaining essentially unchanged. </p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        <a aria-expanded="false" class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
                                           #  Sample Heading
                                        </a>
                                    </div>
                                </div>
                                <div aria-expanded="false" id="collapseFour" role="tabpanel" class="panel-collapse collapse">
                                    <div class="panel-body">
                                         <h3>How To Buy A Product?</h3>
                                        <p>when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centurbut also survived but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the releas survived not  raseth leap into electronic typesetting, remaining essentially unchanged. </p>
                                        <h3>How To Get Product Support?</h3>
                                        <p>when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centurbut also survived but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the releas survived not  raseth leap into electronic typesetting, remaining essentially unchanged. </p>
                                    </div>
                                </div>
                            </div>                            
                        </div>
                    </div>  
                </div>  
            </div> 
            <!-- Help Page End Here --> 
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>

 <?php
 include("footer.php");
 ?>