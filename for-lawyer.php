<?php
include("header.php");
?>

<style>
	.bg-img {
  /* The image used */
  background-image: url("img/flawbanner.jpg");

  min-height: 100vh;
 

  /* Center and scale the image nicely */
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  position: relative;
}

/* Add styles to the form container */
.container1 {
  position: absolute;
  right: 10%;
  border-radius: 5px;
  max-width: 420px;
  padding: 35px;
  background-color: white;
  /*transform: translate(50%,50%);*/
}

/* Full-width input fields */
input[type=text], input[type=password] {
  width: 100%;
  padding: 15px;
  margin: 5px 0 22px 0;
  border: none;
  background: #f1f1f1;
}

input[type=text]:focus, input[type=password]:focus {
  background-color: #ddd;
  outline: none;
}

/* Set a style for the submit button */
.btn {
  background-color: #eac356;
  color: white;
  padding: 16px 20px;
  border: none;
  cursor: pointer;
  width: 100%;
  opacity: 0.9;
}

.btn:hover {
  opacity: 1;
}

input[type=email]{
	width: 100%;
    padding: 15px;
    margin: 5px 0 22px 0;
    border: none;
    background: #f1f1f1;
}

@media screen and (min-width: 300px) and (max-width: 425px){
	.container1 {
    right: 3px;
    margin-top: 45%;
	}

  .banner-text{
    transform: translate(8%,50%) !important;
  }

}


@media screen and (min-width: 426px) and (max-width: 768px){
  .container1 {
    right: 20%;
    margin-top: 35%;
}

  .banner-text{
        transform: translate(22%,50%) !important;
  }
}

@media screen and (min-width: 769px) and (max-width: 1024px){
   .container1 {
    right: 27%;
    margin-top: 24%;
}

  .banner-text{
        transform: translate(46%,40%) !important;
  }

}


@media screen and (min-width: 1025px) and (max-width: 1350px){
   .container1 {
        right: 10%;
    margin-top: 0%;
}

  .banner-text{
        transform: translate(15%,40%) !important;
  }

}


.banner-text h4,h6,h1,h3{
	
	color: white;
	
}

.banner-text{
	transform: translate(50%,50%);
	float: left;
	background-color: rgba(0,0,0,0.5);
	padding: 10px;
	border-radius: 5px;
}

.lf{
	margin-top: 10%;
}

</style>


<div class="bg-img">

	<div class="container-fluid">
		<div class="lf">
		<div class="banner-text">
		<h4 class="hidden-xs">
			Expanded Reach - Online presence - Futuristic Legal Product
		</h4>
		<h6 class="visible-xs">
			Expanded Reach - Online presence - Futuristic Legal Product
		</h6>

		<h1 class="hidden-xs">
			Join our Legal Network.
		</h1>

		<h3 class="visible-xs">
			Join our Legal Network.
		</h3>
	</div>


	<form action="" class="container1">
    <h2>Login</h2>

    <!-- <label for="email"><b>Name</b></label> -->
    <input type="text" placeholder="Enter Name" name="name" required>

    <!-- <label for="email"><b>Email</b></label> -->
    <input type="email" placeholder="Enter Email" name="email" required>

    <!-- <label for="email"><b>Number</b></label> -->
    <input type="text" placeholder="Enter Number" name="number" required>

    <!-- <label for="psw"><b>Password</b></label> -->
    <input type="password" placeholder="Enter Password" name="psw" required>

    <button type="submit" class="btn">Login</button>
  </form>

	</div>
	</div>

	


  
</div>

<?php
include("footer.php");
?>
