<?php
include("header.php");
?>
<style>
    
    .inner-banner-wrapper {
    text-align: center;
    padding: 130px 0 30px;
}

.product-list-view .single-item-list .item-content .item-info .item-title{
    -webkit-box-flex: 3;
    
    -webkit-flex: 3;
    flex: 3;
}

    .item-title .exp{
        padding-top: 20px;

    }

    .page-controls-sorting{
        float: right;
    }

    .product-list-view .single-item-list{
        box-shadow: 0px 0px 2px 0px rgba(205, 214, 222, 0.75);
    }

    .product-list-view .single-item-list .item-img{
        -webkit-box-flex: 3;
    -moz-flex: 3;
    -webkit-flex: 3;
    flex: 3;
    position: relative;
    background-color: white;
    box-shadow: 0px 0px 0px 0px rgba(205, 214, 222, 0.75);
    }

    .product-list-view .single-item-list .item-content{
        box-shadow: 0px 0px 0px 0px rgba(205, 214, 222, 0.75);
    }

    .product-list-view .single-item-list .item-img img{
        padding-top: 15px;
    padding-left: 15px;
    }

</style>

            <!-- Main Banner 1 Area Start Here -->
            <div class="inner-banner-area">
                <div class="container">
                    <div class="inner-banner-wrapper">
                        <p>GET INSTANT ACCESS TO EXPERIENCED LAWYERS</p>
                        <div class="banner-search-area input-group">
                            <input class="form-control" placeholder="Search Your Keywords . . ." type="text">
                            <span class="input-group-addon">
                                <button type="submit">
                                    <span class="glyphicon glyphicon-search"></span>
                                </button>  
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Main Banner 1 Area End Here --> 
            <!-- Inner Page Banner Area Start Here -->
            <div class="pagination-area bg-secondary">
                <div class="container">
                    <div class="pagination-wrapper">
                        <ul>
                            <li><a href="index.php">Home</a><span> -</span></li>
                            <li>Lawyer List</li>
                        </ul>
                    </div>
                </div>  
            </div> 
            <!-- Inner Page Banner Area End Here -->          
            <!-- Product Page Grid Start Here -->
            <div class="product-page-list bg-secondary section-space-bottom">                
                <div class="container">
                    <div class="row">                        
                        <div class="col-lg-9 col-md-8 col-sm-8 col-xs-12 col-lg-push-3 col-md-push-4 col-sm-push-4">
                            <div class="inner-page-main-body">
                                <div class="page-controls">
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="page-controls-sorting">
                                                <div class="dropdown">
                                                    <button class="btn sorting-btn dropdown-toggle" type="button" data-toggle="dropdown">Default Sorting<i class="fa fa-sort" aria-hidden="true"></i></button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="#">Sample</a></li>
                                                        <li><a href="#">Sample</a></li>
                                                        <li><a href="#">Sample</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                                <div class="tab-content">
                                    
                                    <div role="tabpanel" class="tab-pane fade in active clear products-container" id="list-view">
                                        <div class="product-list-view">
                                            <div class="single-item-list">
                                                <div class="item-img">
                                                    <img src="img/product/36.jpg" alt="product" class="img-responsive">
                                                    
                                                </div>
                                                <div class="item-content">
                                                    <div class="item-info">
                                                        <div class="item-title">
                                                            <h3><a href="#">Sample Name</a></h3>
                                                            <span>Designation</span>
                                                            <h3 class="exp">EXPERTISE IN</h3>
                                                            <p>Property Law, Employment, Criminal, Cyber Crime, Immigration Services, Insurance, Corporate, IPR, Startup, Alternate Dispute Resolution, Environment Law, Family </p>
                                                        </div>
                                                        <div class="item-sale-info">
                                                            <div class="price">
                                                                
                                                                <ul class="sidebar-product-btn">
                                                                <li> <a href="#" class="add-to-cart-btn" id="cart-button">Enquire Now</a></li>
                                                                </ul>

                                                            </div>
                                                            <div class="sale-qty"><span style="color: black;font-size: 20px;">4+</span><br>Years of Practice</div>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                            
                                           
                                            
                                           
                                            
                                            
                                            <div class="row">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <ul class="pagination-align-left">
                                                        <li class="active"><a href="#">1</a></li>
                                                        <li><a href="#">2</a></li>
                                                        <li><a href="#">3</a></li>
                                                    </ul>
                                                </div>  
                                            </div>
                                        </div>
                                    </div>                               
                                </div>                                
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 col-lg-pull-9 col-md-pull-8 col-sm-pull-8">
                            <div class="fox-sidebar">
                                <div class="sidebar-item">
                                    <div class="sidebar-item-inner">
                                        <h3 class="sidebar-item-title">Sub Categories</h3>
                                        <ul class="sidebar-categories-list">
                                            <div class="checkbox">
                                                <label><input type="checkbox" value="">Sample</label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" value="">Sample</label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" value="">Sample</label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" value="">Sample</label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" value="">Sample</label>
                                            </div>
                                        </ul>
                                    </div>
                                </div>


                                <div class="sidebar-item">
                                    <div class="sidebar-item-inner">
                                        <h3 class="sidebar-item-title">Gender</h3>
                                        <ul class="sidebar-categories-list">
                                            <div class="checkbox">
                                                <label><input type="checkbox" value="">Male</label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" value="">Female</label>
                                            </div>
                                            
                                        </ul>
                                    </div>
                                </div>


                                <div class="sidebar-item">
                                    <div class="sidebar-item-inner">
                                        <h3 class="sidebar-item-title">Experience</h3>
                                        <ul class="sidebar-categories-list">
                                            <div class="checkbox">
                                                <label><input type="checkbox" value="">20+</label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" value="">16-10</label>
                                            </div>
                                            <div class="checkbox">
                                                <label><input type="checkbox" value="">11-15</label>
                                            </div>

                                            <div class="checkbox">
                                                <label><input type="checkbox" value="">6-10</label>
                                            </div>

                                            <div class="checkbox">
                                                <label><input type="checkbox" value="">0-5</label>
                                            </div>

                                            
                                        </ul>
                                    </div>
                                </div>
                                
                              
                            </div>
                        </div>



                    </div>
                </div>
            </div>
            <!-- Product Page Grid End Here -->
 <?php
 include("footer.php");
 ?>