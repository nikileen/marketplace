<?php
include("header.php");
?>
<style>
    
    .inner-banner-wrapper {
    text-align: center;
    padding: 130px 0 30px;
}

.product-list-view .single-item-list .item-content .item-info .item-title{
    -webkit-box-flex: 3;
    
    -webkit-flex: 3;
    flex: 3;
}

    .item-title .exp{
        padding-top: 20px;

    }

    .page-controls-sorting{
        float: right;
    }

    .product-list-view .single-item-list{
        box-shadow: 0px 0px 2px 0px rgba(205, 214, 222, 0.75);
    }

    .product-list-view .single-item-list .item-img{
        -webkit-box-flex: 3;
    -moz-flex: 3;
    -webkit-flex: 3;
    flex: 3;
    position: relative;
    background-color: white;
    box-shadow: 0px 0px 0px 0px rgba(205, 214, 222, 0.75);
    }

    .product-list-view .single-item-list .item-content{
        box-shadow: 0px 0px 0px 0px rgba(205, 214, 222, 0.75);
    }

    .product-list-view .single-item-list .item-img img{
        padding-top: 15px;
    padding-left: 15px;
    }

    .form-group{
    	border-bottom: 0.5px solid rgba(234, 195, 86,0.9);
    }

    .l1{
        width: 22%;
        float: left;
    }

    .l2{
        width: 35%;
        float: left;
    }

    .head{
        font-weight: 500px;
        list-style-type: square;
        margin-bottom: 10px;
    }

    .subhead{
        padding: 5px 10px;
        margin-left: 20px;
        font-weight: 500;
        font-size: 17px;
        list-style-type: disc;
    }

    .inner-page-padding{
        padding: 0;
    }

    @media (max-width: 668px){
        .l1{
            width: 30%;
        }

        .l2{
            width: 51%
        }
    }

</style>

            <!-- Main Banner 1 Area Start Here -->
            <div class="inner-banner-area">
                <div class="container">
                    <div class="inner-banner-wrapper">
                        <p>GET INSTANT ACCESS TO EXPERIENCED LAWYERS</p>
                        <div class="banner-search-area input-group">
                            <input class="form-control" placeholder="Search Your Keywords . . ." type="text">
                            <span class="input-group-addon">
                                <button type="submit">
                                    <span class="glyphicon glyphicon-search"></span>
                                </button>  
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Main Banner 1 Area End Here --> 

            <!-- Inner Page Banner Area Start Here -->
            <div class="pagination-area bg-secondary">
                <div class="container">
                    <div class="pagination-wrapper">
                        <ul>
                            <li><a href="index.php">Home</a><span> -</span></li>
                            <li>My Profile</li>
                        </ul>
                    </div>
                </div>  
            </div> 
            <!-- Inner Page Banner Area End Here -->  
                    
  			<!-- Profile Page Start Here -->
            <div class="profile-page-area bg-secondary section-space-bottom">                
                <div class="container">
                                       
                                                                     
                   
                    <div class="row profile-wrapper">
                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                            <ul class="profile-title">
                                <li class="active"><a href="#Profile" data-toggle="tab" aria-expanded="false"><i class="fa fa-user" aria-hidden="true"></i>Personal Information</a></li>
                                <li><a href="#Experience" data-toggle="tab" aria-expanded="false"><i class="fa fa-gavel" aria-hidden="true"></i> Experience</a></li>
                                <li><a href="#Education" data-toggle="tab" aria-expanded="false"><i class="fa fa-graduation-cap" aria-hidden="true"></i> Education</a></li>
                                <li><a href="#Certificate" data-toggle="tab" aria-expanded="false"><i class="fa fa-trophy" aria-hidden="true"></i> Certificate</a></li>
                                <li><a href="#Expertise" data-toggle="tab" aria-expanded="false"><i class="fa fa-shield" aria-hidden="true"></i> Expertise</a></li>
                                <li><a href="#qa" data-toggle="tab" aria-expanded="false"><i class="fa fa-question-circle" aria-hidden="true"></i> Q&A</a></li>
                            </ul>
                        </div>
                        <div class="col-lg-9 col-md-8 col-sm-8 col-xs-12"> 
                            <div class="tab-content">
                                <div class="tab-pane fade active in" id="Profile">
                                    <div class="inner-page-details inner-page-content-box">
                                        <h3>Personal Information:</h3>

                                        
                                        <img src="img/profile/5.jpg" style="padding-bottom: 20px;float: right;">
                                        
                                        <h3 style="font-weight: 400;margin-bottom: 10px;"><span style="color: grey;" class="l1">Name: </span>&nbsp; Nikileen Kamble</h3>
                                        <h3 style="font-weight: 400;margin-bottom: 10px;"><span style="color: grey;" class="l1">Email: </span>&nbsp; n@gmail.com</h3>
                                        <h3 style="font-weight: 400;margin-bottom: 10px;"><span style="color: grey;" class="l1">Phone: </span>&nbsp; 9762534668</h3>
                                        <h3 style="font-weight: 400;margin-bottom: 10px;"><span style="color: grey;" class="l1">Gender: </span>&nbsp; Male</h3>
                                        <h3 style="font-weight: 400;margin-bottom: 10px;"><span style="color: grey;" class="l1">City: </span>&nbsp; Pune</h3>
                                        <h3 style="font-weight: 400;margin-bottom: 10px;"><span style="color: grey;" class="l1">Address: </span>&nbsp; Viman nagar, pune</h3>
                                        <h3 style="font-weight: 400;margin-bottom: 10px;"><span style="color: grey;" class="l1">Fees: </span>&nbsp; 1000 Rs</h3>
                                    </div> 
                                </div> 


                                <div class="tab-pane fade" id="Experience">
                                    <div class="inner-page-details inner-page-content-box">
                                        <h3>Experience:</h3>

                                        
                                        
                                        <h3 style="font-weight: 400;margin-bottom: 10px;"><span style="color: grey;" class="l1">Experience: </span>&nbsp; 9+ Years of experience</h3>
                                        <h3 style="font-weight: 400;margin-bottom: 10px;"><span style="color: grey;" class="l1">Court Name: </span>&nbsp; Pune High Court</h3>
                                        <h3 style="font-weight: 400;margin-bottom: 10px;"><span style="color: grey;" class="l1">Start Date: </span>&nbsp; 2019-07-17</h3>
                                        
                                    </div> 
                                </div> 


                                <div class="tab-pane fade" id="Education">
                                    <div class="inner-page-details inner-page-content-box">
                                        <h3>Education:</h3>

                                        
                                        
                                        <h3 style="font-weight: 400;margin-bottom: 10px;"><span style="color: grey;" class="l2">University / College Name: </span>&nbsp; Flame University</h3>
                                        <h3 style="font-weight: 400;margin-bottom: 10px;"><span style="color: grey;" class="l2">Degree: </span>&nbsp; Bachelor of Civil Law</h3>
                                        <h3 style="font-weight: 400;margin-bottom: 10px;"><span style="color: grey;" class="l2">Start Date: </span>&nbsp; 2019-07-17</h3>
                                        <h3 style="font-weight: 400;margin-bottom: 10px;"><span style="color: grey;" class="l2">End Date: </span>&nbsp; 2019-08-17</h3>
                                        
                                    </div> 
                                </div> 


                                <div class="tab-pane fade" id="Certificate">
                                    <div class="inner-page-details inner-page-content-box">
                                        <h3>Certificate:</h3>

                                        
                                        
                                        <h3 style="font-weight: 400;margin-bottom: 10px;"><span style="color: grey;" class="l1">Certificate Name: </span>&nbsp; Certificate Name</h3>
                                        
                                        <h3 style="font-weight: 400;margin-bottom: 10px;"><span style="color: grey;" class="l1">Start Date: </span>&nbsp; 2019-07-17</h3>

                                        <h3 style="font-weight: 400;margin-bottom: 10px;"><span style="color: grey;" class="l1">End Date: </span>&nbsp; 2019-08-17</h3>
                                        
                                    </div> 
                                </div> 


                                <div class="tab-pane fade" id="Expertise">
                                    <div class="inner-page-details inner-page-content-box">
                                        <h3>Expertise:</h3>

                                        
                                        
                                        <ul style="padding-left: 50px;">
                                            <li class="head">Criminal Law</li>
                                            <li class="subhead">Criminal Law</li>
                                            <li class="subhead">Criminal Law</li>
                                            <li class="subhead">Criminal Law</li>
                                            <li class="subhead">Criminal Law</li>
                                            <li class="subhead">Criminal Law</li>

                                        </ul>
                                        
                                    </div> 
                                </div>


                                <div class="tab-pane fade" id="qa">
                                    <div class="inner-page-details inner-page-content-box">

                                        <!-- Help Page Start Here -->
            <div class="help-page-area bg-secondary section-space-bottom">
                <div class="">
                    
                    <div class="inner-page-details inner-page-padding"> 
                        <div class="panel-group help-page-wrapper" id="accordion">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        <a aria-expanded="false" class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                           #  Sample Heading
                                        </a>
                                    </div>
                                </div>
                                <div aria-expanded="false" id="collapseOne" role="tabpanel" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <h3>How To Buy A Product?</h3>
                                        <p>when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centurbut also survived but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the releas survived not  raseth leap into electronic typesetting, remaining essentially unchanged. </p>
                                        <h3>How To Get Product Support?</h3>
                                        <p>when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centurbut also survived but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the releas survived not  raseth leap into electronic typesetting, remaining essentially unchanged. </p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        <a aria-expanded="false" class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                          #  Sample Heading
                                        </a>
                                    </div>
                                </div>
                                <div aria-expanded="false" id="collapseTwo" role="tabpanel" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                         <h3>How To Buy A Product?</h3>
                                        <p>when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centurbut also survived but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the releas survived not  raseth leap into electronic typesetting, remaining essentially unchanged. </p>
                                        <h3>How To Get Product Support?</h3>
                                        <p>when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centurbut also survived but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the releas survived not  raseth leap into electronic typesetting, remaining essentially unchanged. </p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        <a aria-expanded="false" class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                           #  Sample Heading
                                        </a>
                                    </div>
                                </div>
                                <div aria-expanded="false" id="collapseThree" role="tabpanel" class="panel-collapse collapse">
                                    <div class="panel-body">
                                         <h3>How To Buy A Product?</h3>
                                        <p>when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centurbut also survived but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the releas survived not  raseth leap into electronic typesetting, remaining essentially unchanged. </p>
                                        <h3>How To Get Product Support?</h3>
                                        <p>when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centurbut also survived but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the releas survived not  raseth leap into electronic typesetting, remaining essentially unchanged. </p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        <a aria-expanded="false" class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
                                           #  Sample Heading
                                        </a>
                                    </div>
                                </div>
                                <div aria-expanded="false" id="collapseFour" role="tabpanel" class="panel-collapse collapse">
                                    <div class="panel-body">
                                         <h3>How To Buy A Product?</h3>
                                        <p>when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centurbut also survived but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the releas survived not  raseth leap into electronic typesetting, remaining essentially unchanged. </p>
                                        <h3>How To Get Product Support?</h3>
                                        <p>when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centurbut also survived but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the releas survived not  raseth leap into electronic typesetting, remaining essentially unchanged. </p>
                                    </div>
                                </div>
                            </div>                            
                        </div>
                    </div>  
                </div>  
            </div> 
            <!-- Help Page End Here --> 
                                    </div>
                                </div>
                                
                                
                                
                                                                      
                            </div> 
                        </div>  
                    </div>
                </div>
            </div>
            <!-- Profile Page End Here -->  


 <?php
 include("footer.php");
 ?>